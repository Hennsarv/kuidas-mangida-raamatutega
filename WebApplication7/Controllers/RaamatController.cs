﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7;
using WebApplication7.Models;


namespace WebApplication7.Controllers
{
    public class RaamatController : Controller
    {
        private UusRaamatEntities db = new UusRaamatEntities();

        // GET: Raamats
        public ActionResult Index()
        {
            ViewBag.Ostukorv = Ostukorv.Korv;
            return View(db.Raamat.Where(x => x.Nr != 0).ToList());
        }

        public ActionResult Osta(int? id)
        {
            if (id.HasValue)
            {
                
                if (!Ostukorv.Korv.Keys.Contains(id.Value))
                    Ostukorv.Korv.Add(id.Value, new OstukorviRida { RaamatNr = id??0, Mitu = 0 });
                Ostukorv.Korv[id.Value].Mitu++;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Maha(int? id)
        {
            if (id.HasValue)
            {
                if (Ostukorv.Korv.Keys.Contains(id.Value))
                {
                    Ostukorv.Korv[id.Value].Mitu--;
                    if (Ostukorv.Korv[id.Value].Mitu == 0) Ostukorv.Korv.Remove(id.Value);
                }


            }
            return RedirectToAction("Index");
        }

        // GET: Raamats/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Raamat raamat = db.Raamat.Find(id);
            if (raamat == null)
            {
                return HttpNotFound();
            }
            return View(raamat);
        }

        // GET: Raamats/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Raamats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Nr,Nimetus,Autor")] Raamat raamat)
        {
            if (ModelState.IsValid)
            {
                db.Raamat.Add(raamat);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(raamat);
        }

        // GET: Raamats/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Raamat raamat = db.Raamat.Find(id);
            if (raamat == null)
            {
                return HttpNotFound();
            }
            return View(raamat);
        }

        // POST: Raamats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Nr,Nimetus,Autor,Hind")] Raamat raamat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(raamat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(raamat);
        }

        // GET: Raamats/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Raamat raamat = db.Raamat.Find(id);
            if (raamat == null)
            {
                return HttpNotFound();
            }
            return View(raamat);
        }

        // POST: Raamats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Raamat raamat = db.Raamat.Find(id);
            db.Raamat.Remove(raamat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
