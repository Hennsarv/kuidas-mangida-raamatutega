﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7;

namespace WebApplication7
{
    partial class Eksemplar
    {
        string _Nimetus = "";
        string _Autor = "";
        public string Nimetus
        {
            get => RaamatNr == 0 ? _Nimetus : Raamat.Nimetus;
            set => _Nimetus = value;  
        }
        public string Autor
        {
            get => RaamatNr == 0 ? _Autor : Raamat.Autor;
            set => _Autor = value;
        }
    }
}

namespace WebApplication7.Controllers
{
    public class EksemplarController : Controller
    {
        private UusRaamatEntities db = new UusRaamatEntities();

        // GET: Eksemplar
        public ActionResult Index()
        {
            var eksemplar = db.Eksemplar.Include(e => e.Raamat);
            return View(eksemplar.ToList());
        }

        // GET: Eksemplar/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Eksemplar eksemplar = db.Eksemplar.Find(id);
            if (eksemplar == null)
            {
                return HttpNotFound();
            }
            return View(eksemplar);
        }

        // GET: Eksemplar/Create
        //public ActionResult Create()
        //{
        //    ViewBag.RaamatNr = new SelectList(db.Raamat, "Nr", "Nimetus");
        //    Eksemplar e = new Eksemplar();
        //    return View(e);
        //}

        // GET: Eksemplar/Create/id 
        public ActionResult Create(int? id)
        {
            ViewBag.Raamatud = db.Raamat.ToList();


            Raamat r = db.Raamat.Find(id??-1);
            Eksemplar e = new Eksemplar(); 
            if (r == null)
            {
                ViewBag.RaamatNr = new SelectList(db.Raamat, "Nr", "Nimetus");
            }
            else
            {
                e.RaamatNr = r.Nr;
                ViewBag.RaamatNr = new SelectList(db.Raamat, "Nr", "Nimetus", e.RaamatNr);

            }
            return View(e);
        }

        // POST: Eksemplar/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Nr,RaamatNr,Riiul,Nimetus,Autor")] Eksemplar eksemplar)
        {
            if (ModelState.IsValid)
            {
                db.Eksemplar.Add(eksemplar);

                if (eksemplar.RaamatNr == 0)
                {
                    Raamat u = db.Raamat.Where(x => x.Nimetus == eksemplar.Nimetus).Take(1).SingleOrDefault();
                    if (u == null)
                    {
                        db.Raamat.Add(u = new Raamat { Nimetus = eksemplar.Nimetus, Autor = eksemplar.Autor });
                        db.SaveChanges();
                    }
                    u.Eksemplar.Add(eksemplar);
                }
                db.SaveChanges();

                //return RedirectToAction("Detail","Raamat", new { id = eksemplar.RaamatNr });
                return RedirectToAction("Index", "Raamat");
            }

            ViewBag.RaamatNr = new SelectList(db.Raamat, "Nr", "Nimetus", eksemplar.RaamatNr);
            return View(eksemplar);
        }

        // GET: Eksemplar/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Eksemplar eksemplar = db.Eksemplar.Find(id);
            if (eksemplar == null)
            {
                return HttpNotFound();
            }
            ViewBag.RaamatNr = new SelectList(db.Raamat, "Nr", "Nimetus", eksemplar.RaamatNr);
            return View(eksemplar);
        }

        // POST: Eksemplar/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Nr,RaamatNr,Riiul")] Eksemplar eksemplar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eksemplar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RaamatNr = new SelectList(db.Raamat, "Nr", "Nimetus", eksemplar.RaamatNr);
            return View(eksemplar);
        }

        // GET: Eksemplar/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Eksemplar eksemplar = db.Eksemplar.Find(id);
            if (eksemplar == null)
            {
                return HttpNotFound();
            }
            return View(eksemplar);
        }

        // POST: Eksemplar/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Eksemplar eksemplar = db.Eksemplar.Find(id);
            db.Eksemplar.Remove(eksemplar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
