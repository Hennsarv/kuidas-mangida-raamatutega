﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Models
{
    public class OstukorviRida
    {
        UusRaamatEntities db = new UusRaamatEntities();
        public int RaamatNr { get; set; }
        public int Mitu { get; set; }
        public Raamat Raamat => db.Raamat.Find(RaamatNr);


    }

    public class Ostukorv
    {
        public static Dictionary<int, OstukorviRida> Korv = new Dictionary<int, OstukorviRida>();
    }
}